import tkinter as tk

class MainMenu(tk.Frame):
    app = None
    form_data = None
    p1_name_input = None
    p2_name_input = None

    def __init__(self, app):
        self.app = app
        tk.Frame.__init__(self, self.app)
        
        self.game_title_label = tk.Label(self, text = "CHECKERS", font='Helvetica 32 bold', fg="#4d370f")
        self.game_title_label.grid(row = 0, column = 0, columnspan = 2, pady = 64)

        self.p1_name_label = tk.Label(self, text = "Player 1 name:", font='Helvetica 12')
        self.p1_name_label.grid(row = 1, column = 0, padx = 10, pady = 4)

        self.p2_name_label = tk.Label(self, text = "Player 2 name:", font='Helvetica 12')
        self.p2_name_label.grid(row = 2, column = 0, padx = 10, pady = 4)
        
        self.p1_name_input = tk.Entry(self, font='Helvetica 12')
        self.p1_name_input.insert(0, "Player 1")
        self.p1_name_input.grid(row = 1,column = 1, padx = 10, pady = 4)

        self.p2_name_input = tk.Entry(self, font='Helvetica 12')
        self.p2_name_input.insert(0, "Player 2")
        self.p2_name_input.grid(row = 2,column = 1, padx = 10, pady = 4)

        self.start_button = tk.Button(self, width = 10, text='New Game', font='Helvetica 13 bold', background='#E4C999', command=self.startGame)
        self.start_button.grid(row = 3, column = 0, pady=14, sticky=tk.E)

        self.continue_button = tk.Button(self, width = 10, text='Continue', font='Helvetica 13 bold', background='#E4C999', command=self.app.continueGame, state = "disabled")
        self.continue_button.grid(row = 3, column = 1, pady=24)

    def startGame(self):
        self.packFormData()
        self.app.startGame(self.form_data)
        self.continue_button["state"] = "normal"

    def packFormData(self):
        self.form_data = {
            'p1_name': self.p1_name_input.get(),
            'p2_name': self.p2_name_input.get()
        }