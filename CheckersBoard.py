import tkinter as tk
from Tile import *
from Pawn import *

class CheckersBoard(tk.Canvas):
    game = None
    BOARD_WIDTH = 8
    BOARD_HEIGHT = 8
    ROWS_WITH_PAWNS = 3
    TILE_START_X = 50
    TILE_START_Y = 50
    TILE_WIDTH = 50
    TILE_HEIGHT = 50
    RANGE_BLACK = [0, 1, 2]
    RANGE_WHITE = [5, 6, 7]

    selected_tile = None
    locked_pawn = None

    def __init__(self, game):
        tk.Canvas.__init__(self)
        self.game = game

        self.PROMOTION_ROW_WHITE = 0
        self.PROMOTION_ROW_BLACK = self.BOARD_HEIGHT - 1

        self.create_rectangle(
            self.TILE_START_X - 1,
            self.TILE_START_Y - 1,
            self.TILE_START_X + self.BOARD_WIDTH * self.TILE_WIDTH,
            self.TILE_START_Y + self.BOARD_HEIGHT * self.TILE_HEIGHT,
            fill="gray",
            outline="gray"
        )

        self.drawCoordinateTags()
        self.bind('<Button-1>', self.onClick)

    def reset(self):
        self.create_rectangle(0, 0, 500, 28, fill="white", outline="")
        self.create_rectangle(0, 472, 500, 500, fill="white", outline="")
        self.drawPlayerNames()
        self.board = []
        self.game.forgetOldPawns()

        for y in range(self.BOARD_HEIGHT):
            self.board.append([])
            for x in range(self.BOARD_WIDTH):
                tile = Tile(self, x, y)
                if tile.can_hold_pawn and y in self.RANGE_WHITE:
                    tile.pawn = Pawn(tile, self.game.players[0])
                elif tile.can_hold_pawn and y in self.RANGE_BLACK:
                    tile.pawn = Pawn(tile, self.game.players[1])
                self.board[y].append(tile)

    def drawCoordinateTags(self):
        for x in range(self.BOARD_WIDTH):
            self.create_text(
                self.TILE_START_X + int(self.TILE_WIDTH / 2) + self.TILE_WIDTH * x,
                int(self.TILE_HEIGHT / 4 * 3),
                fill="gray", font="Helvetica 10 bold",
                text=f"{self.translateToHumanX(x)}"
            )
            self.create_text(
                self.TILE_START_X + int(self.TILE_WIDTH / 2) + self.TILE_WIDTH * x,
                int(self.TILE_HEIGHT / 4) + self.TILE_START_Y + self.TILE_HEIGHT * self.BOARD_HEIGHT,
                fill="gray", font="Helvetica 10 bold",
                text=f"{self.translateToHumanX(x)}"
            )

        for y in range(self.BOARD_HEIGHT):
            self.create_text(
                int(self.TILE_WIDTH / 4 * 3),
                self.TILE_START_Y + int(self.TILE_HEIGHT / 2) + self.TILE_HEIGHT * y,
                fill="gray", font="Helvetica 10 bold",
                text=f"{self.translateToHumanY(y)}"
            )
            self.create_text(
                int(self.TILE_WIDTH / 4) + self.TILE_START_X + self.TILE_WIDTH * self.BOARD_WIDTH,
                self.TILE_START_Y + int(self.TILE_HEIGHT / 2) + self.TILE_HEIGHT * y,
                fill="gray", font="Helvetica 10 bold",
                text=f"{self.translateToHumanY(y)}"
            )

    def drawPlayerNames(self):
        self.create_text(
            250,
            16,
            fill="gray", font="Helvetica 12 bold",
            text=f"{self.game.players[1].name}"
        )
        self.create_text(
            250,
            484,
            fill="gray", font="Helvetica 12 bold",
            text=f"{self.game.players[0].name}"
        )

    def onClick(self, event):
        if not self.game.is_active:
            return
            
        clicked_tile = self.getClickedTile(event.x, event.y)

        if clicked_tile:
            if clicked_tile.canBeSelected():
                self.selectTile(clicked_tile)
            elif self.selected_tile is not None:
                pawn = self.selected_tile.pawn
                if pawn.canMoveTo(clicked_tile):
                    pawn.moveTo(clicked_tile)
                    self.game.endTurn()
                elif pawn.canCaptureAndMoveTo(clicked_tile):
                    pawn.moveToAndCapture(clicked_tile)
                    pawn.calculatePossibleMoves()
                    if pawn.can_capture:
                        self.locked_pawn = pawn
                        self.selectTile(pawn.tile)
                    else:
                        self.game.endTurn()
                

    def selectTile(self, tile):
        if self.selected_tile is not None:
            self.selected_tile.is_selected = False
            self.selected_tile.draw()
        self.selected_tile = tile
        self.selected_tile.is_selected = True
        self.selected_tile.draw()

    def getTile(self, x, y):
        if self.isInBoardBounds(x, y):
            return self.board[y][x]
        else:
            return None
            
    def getClickedTile(self, x, y):
        x = x - self.TILE_START_X
        y = y - self.TILE_START_Y
        pos_x = int(x / self.TILE_WIDTH)
        pos_y = int(y / self.TILE_HEIGHT)

        return self.getTile(pos_x, pos_y)
        
    def isInBoardBounds(self, x, y):
        return x in range(self.BOARD_WIDTH) and y in range(self.BOARD_HEIGHT)

    def translateToHumanX(self, x):
        return chr(ord('a') + x)

    def translateToHumanY(self, y):
        return self.BOARD_HEIGHT - y