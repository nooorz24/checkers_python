import tkinter as tk

class PlayersFrame(tk.Frame):
    def __init__(self, game):
        tk.Frame.__init__(self, game, highlightbackground="gray", highlightthickness=1)
        self.game = game

        self.p1_name_label = tk.Label(self, font='Helvetica 10 bold')
        self.p1_name_label.grid(row = 0, column = 0, padx = 8, pady = 1)

        self.p1_color_label = tk.Label(self, fg="gray", font='Helvetica 10 italic')
        self.p1_color_label.grid(row = 0, column = 1, padx = 8, pady = 1)

        self.p1_pawn_label = tk.Label(self, text="Pawns:", font='Helvetica 9')
        self.p1_pawn_label.grid(row = 1, column = 0, padx = 8, pady = 1, sticky = tk.E)

        self.p1_pawn_count_label = tk.Label(self, text = "", font='Helvetica 10 bold',)
        self.p1_pawn_count_label.grid(row = 1, column = 1, padx = 8, pady = 1, sticky = tk.W)

        self.p2_name_label = tk.Label(self, font='Helvetica 10 bold')
        self.p2_name_label.grid(row = 2, column = 0, padx = 8, pady = 1)

        self.p2_color_label = tk.Label(self, fg="gray", font='Helvetica 10 italic')
        self.p2_color_label.grid(row = 2, column = 1, padx = 8, pady = 1)

        self.p2_pawn_label = tk.Label(self, text = "Pawns:", font='Helvetica 9')
        self.p2_pawn_label.grid(row = 3, column = 0, padx = 8, pady = 1, sticky = tk.E)

        self.p2_pawn_count_label = tk.Label(self, text = "", font='Helvetica 10 bold')
        self.p2_pawn_count_label.grid(row = 3, column = 1, padx = 8, pady = 1, sticky = tk.W)

    def updateInfo(self):
        self.p1_name_label.configure(text = f"[{self.game.players[0].victories}] {self.game.players[0].name}")
        self.p1_color_label.configure(text = f"{self.game.players[0].assigned_color.upper()}")
        self.p1_pawn_count_label.configure(text=f"{len(self.game.players[0].pawns)}")

        self.p2_name_label.configure(text = f"[{self.game.players[1].victories}] {self.game.players[1].name}")
        self.p2_color_label.configure(text = f"{self.game.players[1].assigned_color.upper()}")
        self.p2_pawn_count_label.configure(text=f"{len(self.game.players[1].pawns)}")