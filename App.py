import tkinter as tk
from MainMenu import *
from Game import *

class App(tk.Tk):
    current_frame = None
    main_menu_frame = None
    game_frame = None

    def __init__(self):
        tk.Tk.__init__(self)
        self.title("Checkers game")
        self.geometry('750x500')
        self.resizable(False, False)
        self.main_menu_frame = MainMenu(self)
        self.createMainMenuFrame()

    def createMainMenuFrame(self):
        if self.game_frame is not None:
            self.game_frame.hideFrame()

        self.main_menu_frame.pack()

    def startGame(self, form_data):
        self.main_menu_frame.pack_forget()
        
        if self.game_frame is None:
            self.game_frame = Game(self, width=750, height=500)

        self.game_frame.startMatch(form_data = form_data)
        self.game_frame.showFrame()

    def continueGame(self):
        self.main_menu_frame.pack_forget()
        self.game_frame.showFrame()
if __name__ == "__main__":
    app = App()
    app.mainloop()
