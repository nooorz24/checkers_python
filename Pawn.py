from Move import *

class Pawn():
    tile = None
    belongs_to = None
    is_queen = False
    can_move = False
    can_capture = False
    is_captured = False
    move_range = 1

    def __init__(self, tile, belongs_to):
        self.tile = tile
        self.belongs_to = belongs_to
        self.belongs_to.pawns.append(self)
        self.draw()
        if self.belongs_to.assigned_color == 'white':
            self.allowed_moving_directions = [
                Move.TOP_RIGHT, Move.TOP_LEFT
            ]
        else:
            self.allowed_moving_directions = [
                Move.BOTTOM_LEFT, Move.BOTTOM_RIGHT
            ]
        self.all_directions = [
            Move.TOP_RIGHT, Move.TOP_LEFT, Move.BOTTOM_LEFT, Move.BOTTOM_RIGHT # capturing is allowed in all directions
        ]

    def getTitle(self):
        if self.is_queen:
            return "Queen"
        else:
            return "Pawn"

    def draw(self):
        self.tile.board.create_oval(
            self.tile.board.TILE_START_X + 5 + self.tile.position["x"] * self.tile.board.TILE_HEIGHT,
            self.tile.board.TILE_START_Y + 5 + self.tile.position["y"] * self.tile.board.TILE_WIDTH,
            self.tile.board.TILE_START_X - 5 + self.tile.position["x"] * self.tile.board.TILE_HEIGHT + self.tile.board.TILE_HEIGHT,
            self.tile.board.TILE_START_Y - 5 + self.tile.position["y"] * self.tile.board.TILE_WIDTH + self.tile.board.TILE_WIDTH,
            fill=self.belongs_to.assigned_color
        )

        if self.is_queen:
            self.drawCrown()

        if self.is_captured:
            self.drawIsCaptured()

    def drawCrown(self):
        board = self.tile.board
        offset_x = board.TILE_START_X +  self.tile.position['x'] * board.TILE_WIDTH
        offset_y = board.TILE_START_Y +  self.tile.position['y'] * board.TILE_HEIGHT
        crown_dots = [
            15,18, 22,27, 25,18, 28,27, 35,18, 30,33, 20,33
        ]

        for index, value in enumerate(crown_dots):
            if index % 2 == 0:
                crown_dots[index] = value + offset_x
            else:
                crown_dots[index] = value + offset_y

        self.tile.board.create_polygon(crown_dots, outline='gray', fill='gold', width=1)

    def drawIsCaptured(self):
        board = self.tile.board
        offset_x = board.TILE_START_X +  self.tile.position['x'] * board.TILE_WIDTH
        offset_y = board.TILE_START_Y +  self.tile.position['y'] * board.TILE_HEIGHT
        cross_dots = [
            15,18, 18,15, 25,22, 32,15, 35,18, 28,25, 35,32, 32,35, 25,28, 18,35, 15,32, 22,25
        ]
        for index, value in enumerate(cross_dots):
            if index % 2 == 0:
                cross_dots[index] = value + offset_x
            else:
                cross_dots[index] = value + offset_y

        self.tile.board.create_polygon(cross_dots, outline='gray', fill='gray', width=1)

    def promoteToQueen(self):
        if not self.is_queen:
            self.move_range = max(self.tile.board.BOARD_WIDTH, self.tile.board.BOARD_HEIGHT)
            self.is_queen = True
            self.allowed_moving_directions = [
                Move.TOP_RIGHT, Move.TOP_LEFT, Move.BOTTOM_LEFT, Move.BOTTOM_RIGHT
            ]
    
    def calculatePossibleMoves(self):
        self.can_capture = False
        self.can_move = False
        distance = 0

        while(True):
            distance +=1
            if distance > self.move_range:
                break
            directions_with_limits_reached = []
                
            for direction in self.all_directions:
                if direction in directions_with_limits_reached:
                    continue
                    
                pos = self.tile.position
                tile_x = pos['x'] + direction['x'] * distance
                tile_y = pos['y'] + direction['y'] * distance
                tile = self.tile.board.getTile(tile_x, tile_y)
                if tile:
                    if tile.pawn == None:
                        if direction in self.allowed_moving_directions:
                            self.can_move = True
                    else:
                        directions_with_limits_reached.append(direction)
                        if tile.pawn.belongs_to is self.belongs_to.opponent and not tile.pawn.is_captured:
                            if self.tile.board.isInBoardBounds(tile_x + direction['x'], tile_y + direction['y']) and self.tile.board.getTile(tile_x + direction['x'], tile_y + direction['y']).pawn == None:
                                self.can_capture = True
                else: directions_with_limits_reached.append(direction)


    def canMoveTo(self, tile):
        if self.belongs_to.must_capture:
            return False

        self_pos = self.tile.position
        delta_x = tile.position['x'] - self_pos['x']
        delta_y = tile.position['y'] - self_pos['y']
        abs_delta_x = abs(delta_x)
        abs_delta_y = abs(delta_y)
        
        is_on_diagonal = abs_delta_x == abs_delta_y
        
        if not is_on_diagonal:
            return False

        if abs_delta_x == 0 or abs_delta_x > self.move_range:
            return False
            
        step_x = int(delta_x / abs(delta_x))
        step_y = int(delta_y / abs(delta_y))
        
        is_moving_direction_allowed = False
        for direction in self.allowed_moving_directions:
            if direction['x'] == step_x and direction['y'] == step_y:
                is_moving_direction_allowed = True
                break
                
        if not is_moving_direction_allowed:
            return False
            
        for i in range(1, abs_delta_x + 1):
            tile = self.tile.board.getTile(self_pos['x'] + i * step_x, self_pos['y'] + i * step_y)
            if tile.pawn is not None:
                return False

        return True

    def capture(self, captured_pawn):
        captured_pawn.is_captured = True
        captured_pawn.draw()

    def moveTo(self, new_tile):
        old_tile = self.tile
        old_tile.is_selected = False
        old_tile.pawn = None
        new_tile.pawn = self
        self.tile = new_tile

        if self.belongs_to.assigned_color == "white" and self.tile.position['y'] == self.tile.board.PROMOTION_ROW_WHITE:
            self.promoteToQueen()
        elif self.belongs_to.assigned_color == "black" and self.tile.position['y'] == self.tile.board.PROMOTION_ROW_BLACK:
            self.promoteToQueen()

        old_tile.draw()
        new_tile.draw()

        self.tile.board.game.history_frame.logAction(f"{old_tile.position} to {new_tile.position}")

    def canCaptureAndMoveTo(self, tile):
        if tile.pawn is not None:
            return False
        self_pos = self.tile.position
        delta_x = tile.position['x'] - self_pos['x']
        delta_y = tile.position['y'] - self_pos['y']
        abs_delta_x = abs(delta_x)
        abs_delta_y = abs(delta_y)
        
        is_on_diagonal = abs_delta_x == abs_delta_y
        
        if not is_on_diagonal:
            return False
            
        if abs_delta_x == 0 or abs_delta_x > self.move_range + 1:
            return False
            
        step_x = int(delta_x / abs(delta_x))
        step_y = int(delta_y / abs(delta_y))
        
        for direction in self.allowed_moving_directions:
            if direction['x'] == step_x and direction['y'] == step_y:
                break # finds move direction

        opponents_pieces_in_way = 0
        self_pieces_in_way = 0
        alredy_captured_pieces_in_way = 0
        is_capture_correct = self.is_queen # check is meant not for queens

        for i in range(1, abs_delta_x + 1):
            tmp_tile = self.tile.board.getTile(self_pos['x'] + i * step_x, self_pos['y'] + i * step_y)
            if tmp_tile.pawn is not None:
                if tmp_tile.pawn.belongs_to is self.belongs_to:
                    self_pieces_in_way += 1
                else:
                    opponents_pieces_in_way += 1
                    if tmp_tile.pawn.is_captured:
                        alredy_captured_pieces_in_way += 1
                    if i == abs_delta_x - 1:
                        is_capture_correct = True
                        
        if opponents_pieces_in_way == 1 and self_pieces_in_way == 0 and alredy_captured_pieces_in_way == 0 and is_capture_correct:
            return True
        else:
            return False

    def moveToAndCapture(self, tile):
        self_pos = self.tile.position
        delta_x = tile.position['x'] - self_pos['x']
        delta_y = tile.position['y'] - self_pos['y']
        abs_delta_x = abs(delta_x)
        abs_delta_y = abs(delta_y)
        step_x = int(delta_x / abs_delta_x)
        step_y = int(delta_y / abs_delta_y)
       

        for i in range(1, abs_delta_x + 1):
            tile = self.tile.board.getTile(self_pos['x'] + i * step_x, self_pos['y'] + i * step_y)
            if tile.pawn is not None:
                self.capture(tile.pawn)

        self.moveTo(tile)