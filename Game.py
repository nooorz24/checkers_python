import tkinter as tk
import datetime

from Player import *
from CheckersBoard import *

from PlayersFrame import *
from HistoryFrame import *
from OptionsFrame import *

class Game(tk.Frame):
    match_nr = 0
    current_move = 0
    players_move = None
    board = None
    players = []
    PAWN_COLORS = ["white", "black"]
    is_active = True
    def __init__(self, app, width=750, height=500):
        self.app = app
        tk.Frame.__init__(self, self.app, width=750, height=500)
        
        self.board = CheckersBoard(self)
        self.board.place(x=0, y=0, anchor="nw", width=500, height=500)

        self.players_frame = PlayersFrame(self)
        self.players_frame.place(x=500, y=0, anchor="nw", width=250, height=100)

        self.history_frame = HistoryFrame(self)
        self.history_frame.place(x=500, y=100, anchor="nw", width=250, height=300)

        self.options_frame = OptionsFrame(self)
        self.options_frame.place(x=500, y=400, anchor="nw", width=250, height=100)

    def startMatch(self, form_data = None):
        if form_data:
            self.match_nr = 0
            self.players = [
                Player(self, form_data["p1_name"], self.PAWN_COLORS[0]),
                Player(self, form_data["p2_name"], self.PAWN_COLORS[1])
            ]
            
            self.players[0].opponent = self.players[1]
            self.players[1].opponent = self.players[0]

            self.createLogFile()

        self.match_nr += 1
        if self.match_nr > 1:
            self.swapPlayers()
            
        self.board.reset()

        self.current_move = 0
        self.players_move = self.players[0]
        self.players_move.calculatePawnMoveOptions()

        self.options_frame.enableSurrenderButton()
        self.options_frame.disableNextRoundButton()
        self.history_frame.writeToLogFile(f"[{self.players[0].assigned_color.upper()}] - Name: {self.players[0].name}")
        self.history_frame.writeToLogFile(f"[{self.players[1].assigned_color.upper()}] - Name: {self.players[1].name}\n")

        self.players_frame.updateInfo()
        self.is_active = True

    def surrender(self):
        self.players_move.surrender()

    def swapPlayers(self):
        tmpPlayer = self.players[0]
        self.players[0] = self.players[1]
        self.players[1] = tmpPlayer

        self.players[0].assignColor(self.PAWN_COLORS[0])
        self.players[1].assignColor(self.PAWN_COLORS[1])

    def createLogFile(self):
        datetimeString = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
        self.log_file_name = f"game_logs/game_{datetimeString}.txt"
        f = open(self.log_file_name, 'a+')  # open file in append mode
        f.close()

    def endGame(self, is_surrender=False):
        if is_surrender:
            self.history_frame.logAction(f" SURRENDERED")
            winner = self.players_move.opponent
        else:
            self.history_frame.logAction(f" is a WINNER!!!")
            winner = self.players_move

        winner.victories += 1
        self.is_active = False
        self.options_frame.disableSurrenderButton()
        self.options_frame.enableNextRoundButton()
        
    def endTurn(self):
        self.finishTurn()

        if not self.players_move.opponent.hasPawnsLeft():
            self.endGame()
            return

        self.startNextPlayersTurn()

    def forgetMetaData(self):
        self.board.selected_tile = None
        self.board.locked_pawn = None
        for player in self.players:
            player.must_capture = False

    def finishTurn(self):
        self.forgetMetaData()
        self.current_move += 1

        for pawn in self.players_move.pawns:
            pawn.can_move = False
            pawn.can_capture = False
            pawn.tile.draw()

        for pawn in self.players_move.opponent.pawns:
            if pawn.is_captured:
                tile = pawn.tile
                tile.pawn = None
                tile.draw()

        self.players_move.opponent.pawns = [pawn for pawn in self.players_move.opponent.pawns if not pawn.is_captured]

        self.players_frame.updateInfo()

    def startNextPlayersTurn(self):
        self.players_move = self.players[self.current_move % 2]
        self.players_move.calculatePawnMoveOptions()
        
        if self.players_move.must_capture:
            for pawn in self.players_move.pawns:
                if pawn.can_capture:
                    pawn.tile.draw()

    def quitToMainMenu(self):
        self.board.destroy()
        self.app.createMainMenuFrame()

    def hideFrame(self):
        self.board.place_forget()
        self.pack_forget()

    def showFrame(self):
        self.pack()
        self.board.place(x=0, y=0, anchor="nw", width=500, height=500)

    def forgetOldPawns(self):
        for player in self.players:
            player.pawns = []