import tkinter as tk

class OptionsFrame(tk.Frame):
    def __init__(self, game):
        tk.Frame.__init__(self, game, background="orange")
        self.game = game
        self.surrender_button = tk.Button(self, width = 12, text='Surrender', font='Helvetica 10 bold', background='#E4C999', command=self.game.surrender) 
        self.surrender_button.grid(row = 0, column = 0, pady=6, padx=6)

        self.next_round_button = tk.Button(self, width = 12, text='Next round', font='Helvetica 10 bold', background='#E4C999', command=self.game.startMatch) 
        self.next_round_button.grid(row = 0, column = 1, pady=6, padx=6)

        self.main_menu_button = tk.Button(self, width = 12, text='Main menu', font='Helvetica 10 bold', background='#E4C999', command=self.game.app.createMainMenuFrame) 
        self.main_menu_button.grid(row = 1, column = 0, pady=6, padx=6)

    def disableSurrenderButton(self):
        self.surrender_button["state"] = "disabled"

    def enableSurrenderButton(self):
        self.surrender_button["state"] = "normal"

    def disableNextRoundButton(self):
        self.next_round_button["state"] = "disabled"

    def enableNextRoundButton(self):
        self.next_round_button["state"] = "normal"