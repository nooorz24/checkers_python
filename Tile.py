class Tile():
    COLOR_BLACK = "#75653d"
    COLOR_WHITE = "#faf4be"
    color_tag = None
    can_hold_pawn = False
    pawn = None
    board = None
    is_selected = False
    position = None
    coord_start_x = 0
    coord_start_y = 0
    coord_end_x = 0
    coord_end_y = 0
    IS_SELECTED_MARKR_COLOR = "#22FF44"
    CAN_PAWN_CAPTURE_MARKR_COLOR = "#B59B19"

    def __init__(self, board, x, y):
        self.position = {"x": x, "y": y}
        self.board = board
        self.can_hold_pawn = bool((self.position["x"] + self.position["y"]) % 2)
        self.draw()

    def draw(self):
        if self.can_hold_pawn:
            fill_color = self.COLOR_BLACK
        else:
            fill_color = self.COLOR_WHITE

        self.coord_start_x = self.board.TILE_START_X + self.position["x"] * self.board.TILE_WIDTH
        self.coord_start_y = self.board.TILE_START_Y + self.position["y"] * self.board.TILE_HEIGHT
        self.coord_end_x = self.board.TILE_START_X + self.position["x"] * self.board.TILE_WIDTH + self.board.TILE_WIDTH
        self.coord_end_y = self.board.TILE_START_Y + self.position["y"] * self.board.TILE_HEIGHT + self.board.TILE_HEIGHT

        self.board.create_rectangle(
            self.coord_start_x,
            self.coord_start_y,
            self.coord_end_x,
            self.coord_end_y,
            fill=fill_color,
            outline=""
        )

        if self.is_selected:
            self.drawSelectedMark(self.IS_SELECTED_MARKR_COLOR)
        elif self.pawn and self.pawn.can_capture:
            self.drawSelectedMark(self.CAN_PAWN_CAPTURE_MARKR_COLOR)

        if self.pawn:
            self.pawn.draw()

    def canBeSelected(self):
        check1 = self.pawn is not None and self.pawn.belongs_to == self.board.game.players_move
        check2 = self.pawn is not None and (self.pawn.can_capture or (not self.pawn.belongs_to.must_capture and self.pawn.can_move))
        check3 = self.board.locked_pawn is None

        return check1 and check2 and check3

    def drawSelectedMark(self, fillColor):
        self.board.create_line(
            self.coord_start_x + 13,
            self.coord_start_y + 3,
            self.coord_start_x + 3,
            self.coord_start_y + 3,
            self.coord_start_x + 3,
            self.coord_start_y + 13,
            width=2,
            fill=fillColor
        )

        self.board.create_line(
            self.coord_end_x - 13,
            self.coord_start_y + 3,
            self.coord_end_x - 3,
            self.coord_start_y + 3,
            self.coord_end_x - 3,
            self.coord_start_y + 13,
            width=2,
            fill=fillColor
        )

        self.board.create_line(
            self.coord_start_x + 13,
            self.coord_end_y - 3,
            self.coord_start_x + 3,
            self.coord_end_y - 3,
            self.coord_start_x + 3,
            self.coord_end_y - 13,
            width=2,
            fill=fillColor
        )

        self.board.create_line(
            self.coord_end_x - 13,
            self.coord_end_y - 3,
            self.coord_end_x - 3,
            self.coord_end_y - 3,
            self.coord_end_x - 3,
            self.coord_end_y - 13,
            width=2,
            fill=fillColor
        )