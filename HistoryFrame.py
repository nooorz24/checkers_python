import tkinter as tk

class HistoryFrame(tk.Frame):
    log_lines = []
    log_line_lables = []
    MAX_LOG_LINES = 10
    def __init__(self, game):
        tk.Frame.__init__(self, game, highlightbackground="gray", highlightthickness=1)
        self.game = game

        self.createLogLables()

    def createLogLables(self):
        for i in range (self.MAX_LOG_LINES):
            self.log_lines.append("")
            self.log_line_lables.append(tk.Label(self, text="", font='Helvetica 9'))
            self.log_line_lables[i].grid(row = i, column = 0, padx = 8, pady = 4, sticky = tk.W)

    def logAction(self, text):
        text = f"[{self.game.players_move.assigned_color.upper()}] : {text}"
        self.log_lines.append(text)
        if len(self.log_lines) > self.MAX_LOG_LINES:
            self.log_lines.pop(0)

        self.updateLogLables()
        self.writeToLogFile(text)

    def writeToLogFile(self, text):
        f = open(self.game.log_file_name, 'a+')  # open file in append mode
        f.write(f"{text}\n")
        f.close()

    def updateLogLables(self):
        for i in range(self.MAX_LOG_LINES):
            self.log_line_lables[i].configure(text=f"{self.log_lines[self.MAX_LOG_LINES - i - 1]}")