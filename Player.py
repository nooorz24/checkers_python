class Player():
    name = ""
    game = None
    victories = 0
    assigned_color = None
    must_capture = False
    opponent = None

    def __init__(self, game, name, assigned_color):
        self.game = game
        self.name = name
        self.pawns = []
        self.assignColor(assigned_color)

    def assignColor(self, color):
        self.assigned_color = color

    def hasPawnsLeft(self):
        return len(self.pawns) > 0

    def calculatePawnMoveOptions(self):
        for pawn in self.pawns:
            pawn.calculatePossibleMoves()
            if pawn.can_capture:
                self.must_capture = True

    def surrender(self):
        self.game.endGame(is_surrender=True)